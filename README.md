# Adafruit NeoPixel mit NodeMCU

![Flauschecke](Bilder/Flauschecke.gif "")

## Schaltplan

![Platine](Bilder/Steckplatine.png "")

## Komponenten

- ESP8266 (NodeMCU)
- Netzteil
- WS2812b oder WS2811
- Aderendhülsen


## Werkzeug

- Kreuzschlitz Schraubendreher
- Arduino IDE
- Adafruit NeoPixel Libary
- PubSubClient Libary

## Zusammenbau
- Gnd vom Ledstreifen, Netzteil und NodeMCU verbinden
- Datenkabel vom Ledstreifen an Pin D5 (anpassbar in config) von dem NodeMCU verbinden
- VCC vom Ledstreifen an das Netzteil anschließen

## Software
- Mqtt
- ArduinoIDE

### Steuerung
- Mqtt commands

> rainbowcolor = rainbowcolor EIN - Rest AUS

> dreiercycle = dreiercycle EIN - Rest AUS

> rainbowcycle = rainbowcycle EIN - Rest AUS

> off = Alles AUS

> #(Farbwert in Hex) = Setz den ganzen Streifen in die Farbe

> 1-100 = Setzt beliebigen Speed Wert von 1-100

> %0-255 = Helligkeit

### Konfiguration
1. config-example.h zu config.h umbennen
2. MQTT, LWT und WLAN Settings in der config.h machen
3. Anzahl der Led's und Anschlusspin ggf. in der config.h ändern

### OTA

- über den Webserver mit den festegelegten Parametern in der config
- mit `curl -F "image=@code.ino.bin" http://otaUsername:otaPassword@espHostname:80/update` code.ino.bin ist das kompilierte Arduino sketch und otaUsername, otaPassword@espHostname:otaPort sind in der config


// Neopixel
#include <Adafruit_NeoPixel.h>

//OTA
#include <ArduinoOTA.h>

// ESP 8266
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
extern "C" {
#include "user_interface.h" //to set the hostname
}

#include "config.h"

WiFiClient espClient;
PubSubClient client(espClient);

int brightness = 255;
boolean rainbowcolor = false;
boolean rainbowcycle = false;
boolean dreiercycle = false;
int neopixelspeed = 50;

long lastrainbow = 0;
long lastrainbowcycle = 0;
long last3cycle = 0;
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, NeoPIN, NEO_RGB + NEO_KHZ800);
int j = 0;
int q = 0;

void setup() {
  //Serial start
  Serial.begin ( 115200 );

  // NeoPixel start
  Serial.println();
  strip.setBrightness(brightness);
  strip.begin();
  strip.show();
  delay(50);

  // WiFi setup
  setup_wifi();

  // MQTT connect
  Serial.print("setup MQTT...");
  // connecting to the mqtt server
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  Serial.println("done!");

  client.publish(SpeedTopic, "50");

  // get Start Animation
  if (Animation == 1) {
    rainbowcolor = true;
    Serial.println("Starting with rainbowcolor");
  } else if (Animation == 2) {
    rainbowcycle = true;
    Serial.println("Starting with rainbowcycle");
  } else if (Animation == 3) {
    dreiercycle = true;
    Serial.println("Starting with dreiercycle");
  }
}

void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  wifi_station_set_hostname(espHostname); //set ESP hostname
  WiFi.mode(WIFI_STA); // set Wifi mode
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  //OTA
  ArduinoOTA.setHostname(espHostname);
  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_SPIFFS
      type = "filesystem";
    }

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });
  ArduinoOTA.begin();

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

}

void callback(char* topic, byte* payload, unsigned int length) {

  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  String color("#");
  String command;
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    command = command + String((char)payload[i]);
  }

  Serial.println();

  if (command.toInt() > 0) {
    neopixelspeed = 100 - command.toInt();
  }
  if (command == "rainbowcolor") {
    alloff();
    rainbowcolor = true;
  } else if (command == "rainbowcycle") {
    alloff();
    rainbowcycle = true;
  } else if (command == "dreiercycle") {
    alloff();
    dreiercycle = true;
  } else if (command == "off") {
    alloff();
  }
  // finding Color(hex)payload
  if ((char)payload[0] == '#') {
    // setting color
    alloff();
    setNeoColor(command);
  } else if ((char)payload[0] == '%') {
    command.remove(0, 1);
    strip.setBrightness(command.toInt());
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(willClientID, willTopic, willQoS, willRetain, willOffMessage)) {
      Serial.println("connected");

      // Once connected, publish an announcement...
      client.publish(willTopic, willOnMessage, willRetain);
      // ... and resubscribe
      client.subscribe(mainTopic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void loop() {

  if (!client.connected()) {
    delay(100);
    reconnect();
  }
  client.loop();
  ArduinoOTA.handle();

  long now = millis();
  //RainbowColor
  if (now - lastrainbow > neopixelspeed) {
    lastrainbow = now;
    if (rainbowcolor) {
      if (j < 256)
        j++;
      for (int i = 0; i < strip.numPixels(); i++) {
        strip.setPixelColor(i, Wheel((i + j) & 255));
      }

      strip.show();
      if (j == 256)
        j = 0;


    }
  }

  // Dreiercycle
  if (now - last3cycle > neopixelspeed) {
    last3cycle = now;
    if (dreiercycle) {
      if (j < 256) {
        j++;    // cycle all 256 colors in the wheel
        if (q < 3) {
          q++;
          for (int i = 0; i < strip.numPixels(); i = i + 3) {
            strip.setPixelColor(i + q, Wheel( (i + j) % 255)); //turn every third pixel on
          }
          strip.show();

          for (int i = 0; i < strip.numPixels(); i = i + 3) {
            strip.setPixelColor(i + q, 0);      //turn every third pixel off
          }
        }
        if (j == 256)
          j = 0;

        if (q == 3)
          q = 0;
      }
    }
  }

  //RainbowCycle
  if (now - lastrainbowcycle > neopixelspeed) {
    lastrainbowcycle = now;
    if (rainbowcycle) {
      if (j < 256 * 5)
        j++;
      for (int i = 0; i < strip.numPixels(); i++) {
        strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels()) + j) & 255));
      }


      strip.show();
      if (j == 256)
        j = 0;

    }
  }


  delay(10);


}


void alloff() {
  rainbowcolor = false;
  rainbowcycle = false;
  dreiercycle = false;
  for (int i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, 0, 0, 0);
  }
  strip.show();
}


void setNeoColor(String value) {
  int number = (int) strtol( &value[1], NULL, 16);

  int r = number >> 16;
  int g = number >> 8 & 0xFF;
  int b = number & 0xFF;

  Serial.print("RGB: ");
  Serial.print(r, DEC);
  Serial.print(" ");
  Serial.print(g, DEC);
  Serial.print(" ");
  Serial.print(b, DEC);
  Serial.println(" ");

  for (int i = 0; i < NUM_LEDS; i++) {
    strip.setPixelColor(i, strip.Color( g, r, b ) );
  }
  strip.show();

}


// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if (WheelPos < 85) {
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if (WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}

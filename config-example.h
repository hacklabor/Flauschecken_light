// WiFi Settings
const char* ssid = "WLAN-SSID";
const char* password = "WLAN Passwort";
char* espHostname = "hostame das ESP's";

// MQTT Server
const char* mqtt_server = "MQTT Broker";

// MQTT Subscribes
const char* mainTopic = "Topic/#";
const char* SpeedTopic = "Topic/Speed";

//Start Animation (1-3)
// 1=rainbowcolor, 2=rainbowcycle, 3=dreiercycle
const int Animation = 1;

// MQTT Last will and Testament
byte willQoS = 0;
const char* willTopic = "lwt/esp-topic";
const char* willOnMessage = "online";
const char* willOffMessage = "offline";
const char* willClientID = "esp-ClientID";
boolean willRetain = true;

//OTA
boolean otaActive = true;
const char* otaUsername = "username";
const char* otaPassword = "password";
const int otaPort = 80;

// Neopixel Config
#define NeoPIN D5
#define NUM_LEDS 176

